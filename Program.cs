﻿//NET Class Library
//=================

//Llamada a métodos
Random dice = new Random();
int roll = dice.Next(1,10);
Console.WriteLine(roll);

//Métodos sobrecargados
Random aleatorio = new Random();
int roll1 = aleatorio.Next();
int roll2 = aleatorio.Next(100);
int roll3 = aleatorio.Next(50, 100);

Console.WriteLine($"First roll: {roll1}");
Console.WriteLine($"Second roll: {roll2}");
Console.WriteLine($"Third roll: {roll3}");

//Challenge
//Método de la clase System.Math para determinar número mayor
int firstValue = 1500;
int secondValue = 700;
int largerValue;
largerValue = Math.Max(firstValue, secondValue);
Console.WriteLine($"El número mayor es: {largerValue}");